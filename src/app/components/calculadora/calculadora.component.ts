import { Component, OnInit } from '@angular/core';
import { Form, FormArray, FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-calculadora',
  templateUrl: './calculadora.component.html',
  styleUrls: ['./calculadora.component.css']
})
export class CalculadoraComponent implements OnInit {

  calculadora!:FormGroup;
  constructor(private fb:FormBuilder) {
    this.crearCalculadora();
   }

  ngOnInit(): void {
  console.log(this.calculadora.get('operadores') as FormArray);

  }


  get operadoreS(){

    //Convertir = castear;
    return this.calculadora.get('operadores') as FormArray;
    
  }

  get numberS(){

    //Convertir = castear;
    return this.calculadora.get('numbers') as FormArray;
  }

  crearCalculadora(){
    this.calculadora = this.fb.group({
      numbers:this.fb.array([0,1,2,3,4,5,6,7,8,9]),
      operadores:this.fb.array(['+','-','*','/'])

    })
  }

  operador(e:any){
    console.log(e.value);
    
  }

}
